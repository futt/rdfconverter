#!/usr/bin/env python3
# -*- coding: utf-8 -*-
"""
 Convert tab-separated movie data from IMDB into RDF triplets in Turtle format
 Please see README.md for instructions and requirements.

 AUTHORS:
   - Candidate #152
   - Candidate #165
   - Candidate #114
"""
import csv
from rdflib import Graph, Namespace, Literal, URIRef
from rdflib.namespace import RDF, DC, FOAF
import wget
import os.path
import gzip
import shutil
from tqdm import tqdm
import ast


def main():
    # DATA_DIR is the location of the raw .tsv data from IMDB (uncompressed)
    # Don't touch DATA_URL and DATA_FILES unless there is a good reason to.
    DATA_DIR='./data/'
    DATA_URL='https://datasets.imdbws.com/'
    DATA_FILES=['name.basics.tsv', 'title.basics.tsv', 
                'title.principals.tsv','title.ratings.tsv']

    # Define filter parameters
    F_ADULT = False                 # Include movies flagged as 'adult' - i.e. porn
    F_MIN_YEAR = 2000               # Include only movies released after this year
    F_MIN_RUNTIME = 60              # Minimum runtime of the movie (exclude shorter)
    F_MIN_VOTES = 10000             # Minimum number of rating votes
    
    # List of content-types to include
    F_TYPES = ['movie', 'tvMovie']

    # We're constructing the graph in memory... This requires a lot of RAM!
    g = Graph()
    
    # Namespaces
    IMDB = Namespace("http://www.csd.abdn.ac.uk/~ggrimnes/dev/imdb/IMDB#")
    REV = Namespace("http://purl.org/stuff/rev#")
    QUIZ = Namespace("https://bitbucket.org/futt/quizapp#")

    g.bind('rdf', RDF)
    g.bind('foaf', FOAF)
    g.bind('imdb', IMDB)
    g.bind('rev', REV)
    g.bind('dc', DC)
    g.bind('quiz', QUIZ)

    # Download and uncompress the tab-separated data from IMDB
    print("Acquiring data files:")
    for F in DATA_FILES:
        if(os.path.isfile(DATA_DIR + F)):
            # File already exists
            print(" Skipping %s, already present." % F)
        else:
            # Download file
            print(" Downloading %s.gz ..." %F)
            wget.download(DATA_URL + F + '.gz', DATA_DIR + F + '.gz')
            # ..and uncompress it, writing the uncompressed data to a new file.
            # We'll treat these as binary files to avoid any encoding issues and such
            print("\n Uncompressing %s.gz ..." %F)
            with gzip.open(DATA_DIR + F + '.gz', 'rb') as gzfd:
                with open(DATA_DIR + F, 'wb') as outfd:
                    shutil.copyfileobj(gzfd, outfd)

    # Create the graph using the acquired TSV files
    print("\nCreating RDF graph from IMDB source data, please wait. A lot.")

    # Parse 'title.basics.tsv'. Fields:
    # tconst,titleType,primaryTitle,originalTitle,isAdult,startYear,endYear,runtimeMinutes,genres
    with open(DATA_DIR + "title.basics.tsv", "r", encoding="utf-8", errors="ignore") as fd:
        tsvreader = csv.reader(fd, delimiter="\t", quotechar='"')
        count = sum(1 for _ in fd)  # Find number of lines in file
        fd.seek(0, 0)

        print("(1/4) Parsing title.basics.tsv")
        pbar = tqdm(total=count)

        for row in tsvreader:
            # Make sure we have valid values for some crucial fields.
            if not str.isdigit(row[5]):     row[5] = "0"
            if not str.isdigit(row[6]):     row[6] = "0"
            if not str.isdigit(row[7]):     row[7] = "0"

            # Check filters to see if this movie should be included
            if((F_ADULT or row[4] == '0') and 
                (int(row[5]) >= F_MIN_YEAR ) and 
                (int(row[7]) >= F_MIN_RUNTIME) and
                (row[1] in F_TYPES)):

                # Find the columns we want to use for the RDF data
                m_id = row[0]           # Unique ID
                m_type = row[1]         # Title type
                m_title = row[2]        # Title
                m_startyear = row[5]    # Production start year
                m_genres = row[8]       # Comma-separated list of genres.

                # Create RDF triples in graph
                m_uri = URIRef("http://www.imdb.com/title/%s/" % m_id)
                g.add((m_uri, RDF.type, IMDB[m_type]))              # Set type
                g.add((m_uri, DC.title, Literal(m_title)))          # Set title
                g.add((m_uri, IMDB.year, Literal(m_startyear)))     # Set start/release year

                # Add genres
                for gen in m_genres.split(","):
                    g.add((m_uri, IMDB.genre, Literal(gen)))        # Add genre
            else:
                pass    # Can most likely be omitted, we're not doing anything with this.
            pbar.update(1)

        # Wrap it up
        fd.close()
        pbar.close()


    # Parse 'title.ratings.tsv'. Fields:
    # tconst,averageRating,numVotes
    with open(DATA_DIR + "title.ratings.tsv", "r", encoding="utf-8", errors="ignore") as fd:
        tsvreader = csv.reader(fd, delimiter="\t", quotechar='"')
        count = sum(1 for _ in fd)  # Find number of lines in file
        fd.seek(0, 0)

        print("(2/4) Parsing title.ratings.tsv")
        pbar = tqdm(total=count)

        for row in tsvreader:
            # Make sure we have valid values for some crucial fields.
            if not str.isdigit(row[2]):     row[2] = "0"

            m_uri = URIRef("http://www.imdb.com/title/%s/" % row[0])

            # Check if this is a movie we have in our graph
            if (m_uri, None, None) in g:
                # Check if the movie has the required number of votes
                if(int(row[2]) >= F_MIN_VOTES):
                    m_avgrating = row[1]
                    m_numvotes = row[2]
                
                    # Add information to graph
                    g.add((m_uri, REV.Rating, Literal(m_avgrating)))        # Average rating
                    g.add((m_uri, REV.TotalVotes, Literal(m_numvotes)))     # Number of votes|
                else:
                    # Too few ratings; we'll just remove it from the graph.
                    g.remove((m_uri, None, None))
            pbar.update(1)
        
        # Wrap it up
        fd.close()
        pbar.close()


    # Parse 'title.principals.tsv'. Fields:
    # tconst,ordering,nconst,category,job,characters
    with open(DATA_DIR + "title.principals.tsv", "r", encoding="utf-8", errors="ignore") as fd:
        tsvreader = csv.reader(fd, delimiter="\t", quotechar='"')
        count = sum(1 for _ in fd)  # Find number of lines in file
        fd.seek(0, 0)

        print("(3/4) Parsing title.principals.tsv")
        pbar = tqdm(total=count)

        for row in tsvreader:
            m_uri = URIRef("http://www.imdb.com/title/%s/" % row[0])

            # Check if this is a movie we have in our graph
            if (m_uri, None, None) in g:
                m_principal = row[2]
                m_order = row[1]
                m_pcat = row[3]

                # This is the URIRef for the principal; we might add it to the graph later...
                m_puri = URIRef("http://www.imdb.com/name/%s/" % m_principal)

                # Add information to graph if relevant
                if(m_pcat == "director"):
                    # This is the movie director
                    g.add((m_uri, IMDB.director, m_puri))
                elif((m_pcat == "actor" or m_pcat == "actress") and m_order == "1"):
                    # This is the lead actor/actress
                    g.add((m_uri, IMDB.actor, m_puri))
                    # Check if characters is empty; if not, parse the list and create roles
                    if(row[5] != '\\N'):
                        for role in ast.literal_eval(row[5]):
                            g.add((m_uri, IMDB.role, Literal(role)))
                else:
                    pass    # Useless info, moving on!
            else:
                pass    # Can probably just skip; irrelevant
            pbar.update(1)
        
        # Wrap it up
        fd.close()
        pbar.close()
    

    # Parse 'name.basics.tsv'. Fields:
    # nconst,primaryName,birthYear,deathYear,primaryProfession,knownForTitles
    with open(DATA_DIR + "name.basics.tsv", "r", encoding="utf-8", errors="ignore") as fd:
        tsvreader = csv.reader(fd, delimiter="\t", quotechar='"')
        count = sum(1 for _ in fd)  # Find number of lines in file
        fd.seek(0, 0)

        print("(4/4) Parsing name.basics.tsv")
        pbar = tqdm(total=count)

        for row in tsvreader:
            p_uri = URIRef("http://www.imdb.com/name/%s/" % row[0])

            # Check if this is a person we have in our graph
            if (None, None, p_uri) in g:
                p_name = row[1]

                g.add((p_uri, RDF.type, FOAF['Person']))            # Set type
                g.add((p_uri, FOAF.name, Literal(p_name)))          # Set name

                # Check gender if actor/actress and tag the person accordingly
                # NOTE: The FOAF namespace spec does include a 'gender' property but
                # this property is missing from the RDFLib implementation as of 5.0.0
                # See: https://github.com/RDFLib/rdflib/issues/1210
                if("actor" in row[4].split(",")):
                    g.add((p_uri, QUIZ.gender, Literal("male")))    # Set gender (male)
                elif("actress" in row[4].split(",")):
                    g.add((p_uri, QUIZ.gender, Literal("female")))  # Set gender (female)
            else:
                pass    # Can probably just skip; irrelevant
            pbar.update(1)
        
        # Wrap it up
        fd.close()
        pbar.close()


    # Done! Save complete graph to turtle file
    print("Serializing graph, please wait, this will take a while...")
    g.serialize(destination='graph.ttl', format='turtle')


if __name__== "__main__":
    main()
