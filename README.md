# README #

Convert tab-separated movie data from IMDB into RDF triplets in Turtle format

### REQUIREMENTS: ###

The required python modules can be found in requirements.txt. Prior to
running this script, please make sure they are installed:

> pip install -r requirements.txt

### USAGE: ###

If you want to use a different data folder, change DATA_DIR to reference the 
folder you want the datafiles to be in. Install the requirements, then simply 
run this script.

> python ./rdfconverter.py

The resulting RDF triples are stored in 'graph.ttl'.

This process will take a long time. On my (admittedly not exceptional) computer,
the process took around 45 minutes with the default filter settings and data 
files already downloaded. If you wish to reduce this a bit, you may increase the 
F_MIN_YEAR variable to include a more limited selection of movies.

### WARNING!! ###

* Please make sure you have at least 4GB of harddrive space available for
the data files from IMDB (downloads + space to uncompress)
* Generating the graph requires a lot of RAM, how much will depend on your
filter settings (more movies = bigger graph). Exit any running
applications you don't need, and make sure you have some swap space
available just in case. Keep an eye on RAM consumption and terminate
the program if you get dangerously low on memory to avoid system
instability.
